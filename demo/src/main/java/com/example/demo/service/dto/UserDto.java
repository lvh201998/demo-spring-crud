package com.example.demo.service.dto;

import com.example.demo.persistence.entity.Role;
import com.example.demo.persistence.entity.User;
import lombok.Data;
import vn.com.itechcorp.base.service.dto.DtoGet;

import java.util.Collection;

@Data
public class UserDto extends DtoGet<User, Long> {

    private String username;
    private String password;

    private Collection<Role> roleList;

    @Override
    public void parse(User user) {

    }
}
