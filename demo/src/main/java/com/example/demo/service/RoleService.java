package com.example.demo.service;

import com.example.demo.persistence.entity.Role;
import com.example.demo.service.dto.RoleDto;
import vn.com.itechcorp.base.service.BaseDtoService;

public interface RoleService extends  BaseDtoService<RoleDto, Role, Long> {
}
