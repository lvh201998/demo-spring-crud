package com.example.demo.service.impl;

import com.example.demo.persistence.UserRepository;
import com.example.demo.persistence.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.service.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.com.itechcorp.base.persistence.repository.BaseRepository;
import vn.com.itechcorp.base.service.impl.BaseDtoJpaServiceImpl;

@Service
@AllArgsConstructor
public class UserServiceImpl extends BaseDtoJpaServiceImpl<UserDto, User, Long> implements UserService {
    @Autowired
    private UserRepository userRepository;

    private final ObjectMapper objectMapper;

    @Override
    public BaseRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    public UserDto convert(User user) {
        return objectMapper.convertValue(user, UserDto.class);
    }

    @Override
    public UserDto getByUsername(String username) {
        User user = userRepository.findFirstByUsername(username).orElse(null);
        if (user != null) return convert(user);
        return null;
    }
}
