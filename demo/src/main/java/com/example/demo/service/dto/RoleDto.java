package com.example.demo.service.dto;

import com.example.demo.persistence.entity.Role;
import lombok.Data;
import vn.com.itechcorp.base.service.dto.DtoGet;

@Data
public class RoleDto extends DtoGet<Role, Long> {
    private String name;

    @Override
    public void parse(Role role) {

    }
}
