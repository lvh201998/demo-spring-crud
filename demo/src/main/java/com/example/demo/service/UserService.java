package com.example.demo.service;

import com.example.demo.persistence.entity.User;
import com.example.demo.service.dto.UserDto;
import vn.com.itechcorp.base.service.BaseDtoService;

public interface UserService extends  BaseDtoService<UserDto, User, Long> {

    UserDto getByUsername(String username);

}
