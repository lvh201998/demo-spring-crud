package com.example.demo.service.impl;

import com.example.demo.persistence.RoleRepositoty;
import com.example.demo.persistence.entity.Role;
import com.example.demo.service.RoleService;
import com.example.demo.service.dto.RoleDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.com.itechcorp.base.persistence.repository.BaseRepository;
import vn.com.itechcorp.base.service.impl.BaseDtoJpaServiceImpl;

@Service
@AllArgsConstructor
public class RoleServiceImpl extends BaseDtoJpaServiceImpl<RoleDto, Role, Long> implements RoleService {

    @Autowired
    private RoleRepositoty roleRepositoty;

    private final ObjectMapper objectMapper;

    @Override
    public BaseRepository<Role, Long> getRepository() {
        return roleRepositoty;
    }

    @Override
    public RoleDto convert(Role role) {
        return objectMapper.convertValue(role, RoleDto.class);
    }
}
