package com.example.demo.persistence.entity;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import vn.com.itechcorp.base.persistence.model.BaseSerialIDEntry;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table
@Data
public class User extends BaseSerialIDEntry {

    @Column
    private String username;
    @Column
    private String password;

    public User(String username, String password, List<Role> roleList) {
        this.username = username;
        this.password = password;
        this.roleList = roleList;
    }


    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE})
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Collection<Role> roleList;

    public User() {

    }
}
