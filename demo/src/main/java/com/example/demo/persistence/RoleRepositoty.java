package com.example.demo.persistence;

import com.example.demo.persistence.entity.Role;
import vn.com.itechcorp.base.persistence.repository.BaseRepository;

public interface RoleRepositoty extends BaseRepository<Role, Long> {
}
