package com.example.demo.persistence;

import com.example.demo.persistence.entity.User;
import vn.com.itechcorp.base.persistence.repository.BaseRepository;

import java.util.Optional;

public interface UserRepository extends BaseRepository<User, Long> {
    Optional<User> findFirstByUsername(String s);
}
