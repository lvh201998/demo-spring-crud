package com.example.demo.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.com.itechcorp.base.persistence.model.BaseSerialIDEntry;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "role")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseSerialIDEntry {
    private String name;
}
