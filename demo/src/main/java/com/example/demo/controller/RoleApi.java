package com.example.demo.controller;

import com.example.demo.controller.request.RoleRequest;
import com.example.demo.persistence.entity.Role;
import com.example.demo.service.RoleService;
import com.example.demo.service.dto.RoleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.com.itechcorp.base.api.method.AsyncBaseDtoAPIMethod;
import vn.com.itechcorp.base.api.response.APIListResponse;
import vn.com.itechcorp.base.api.response.APIResponse;
import vn.com.itechcorp.base.service.filter.PaginationInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/role")
public class RoleApi extends AsyncBaseDtoAPIMethod<RoleDto, Role, Long>{

    public RoleApi(RoleService roleService) {
        super(roleService);
    }

    @GetMapping("/all")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public CompletableFuture<ResponseEntity<APIListResponse<List<RoleDto>>>> getAll() {
        return getListAsync(new PaginationInfo());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public CompletableFuture<ResponseEntity<APIResponse<RoleDto>>> getById(@PathVariable long id) {
        return getByIdAsync(id);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ROLE_USER')")
    public CompletableFuture<ResponseEntity<APIResponse<RoleDto>>> create(@RequestBody RoleRequest roleRequest) {
        return createAsync(roleRequest);
    }

    @DeleteMapping("/{id}")
    public CompletableFuture<ResponseEntity<Object>> Delete(@PathVariable long id) {
        return deleteAsync(id);
    }
}
