package com.example.demo.controller;

import com.example.demo.controller.request.UserRequest;
import com.example.demo.persistence.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.service.dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.com.itechcorp.base.api.method.AsyncBaseDtoAPIMethod;
import vn.com.itechcorp.base.api.response.APIListResponse;
import vn.com.itechcorp.base.api.response.APIResponse;
import vn.com.itechcorp.base.service.filter.PaginationInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/user")
public class UserApi extends AsyncBaseDtoAPIMethod<UserDto, User, Long> {

    public UserApi(UserService userService) {
        super(userService);
    }

    @GetMapping("/all")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public CompletableFuture<ResponseEntity<APIListResponse<List<UserDto>>>> getAll() {
        return getListAsync(new PaginationInfo());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public CompletableFuture<ResponseEntity<APIResponse<UserDto>>> getUserById(@PathVariable long id) {
        return getByIdAsync(id);
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_USER')")
    public CompletableFuture<ResponseEntity<APIResponse<UserDto>>> createUser(@RequestBody UserRequest userRequest) {
        return createAsync(userRequest);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public CompletableFuture<ResponseEntity<Object>> DeleteUser(@PathVariable long id) {
        return deleteAsync(id);
    }
}
