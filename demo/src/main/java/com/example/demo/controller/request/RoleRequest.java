package com.example.demo.controller.request;

import com.example.demo.persistence.entity.Role;
import vn.com.itechcorp.base.service.dto.BaseDtoCreate;

public class RoleRequest extends BaseDtoCreate<Role, Long> {
    private String name;

    @Override
    public Role toEntry() {
        return new Role(name);
    }

    @Override
    public Long getId() {
        return null;
    }
}
