package com.example.demo.controller.request;

import com.example.demo.persistence.entity.Role;
import com.example.demo.persistence.entity.User;
import lombok.AllArgsConstructor;
import vn.com.itechcorp.base.service.dto.BaseDtoCreate;

import java.util.List;

@AllArgsConstructor
public class UserRequest extends BaseDtoCreate<User, Long> {

    private String username;
    private String password;

    private List<Role> roleList;

    @Override
    public User toEntry() {
        return new User(username, password, roleList );
    }

    @Override
    public Long getId() {
        return null;
    }
}
