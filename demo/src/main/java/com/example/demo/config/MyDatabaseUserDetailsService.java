package com.example.demo.config;

import com.example.demo.persistence.UserRepository;
import com.example.demo.persistence.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.service.dto.UserDto;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class MyDatabaseUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto user= userService.getByUsername(username);
        if(Objects.isNull(user)){
            throw new Exception("Not found username " + username);
        }
        List<GrantedAuthority> roleList = new ArrayList<>();
        user.getRoleList().forEach(role -> {
            roleList.add(new SimpleGrantedAuthority("ROLE_" +role.getName()));
        });

        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),roleList);
    }
}

